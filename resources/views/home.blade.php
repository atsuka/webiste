@extends('layouts/dashboard')
@section('menu')
<div class="content-side content-side-full">
    <ul class="nav-main">
        <li class="nav-main-item">
            <a class="nav-main-link active" href="#">
                <i class="nav-main-link-icon si si-speedometer"></i>
                <span class="nav-main-link-name">Dashboard</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link active" href="" data-toggle="modal" data-target="#modal-block-popin">
                <i class="nav-main-link-icon  far fa-address-card fa-2x "></i>
                <span class="nav-main-link-name">Profil</span>
            </a>
            <a class="nav-main-link active" href="" data-toggle="modal" data-target="#modal-block-jenis">
                <i class="nav-main-link-icon  far fa-keyboard fa-2x "></i>
                <span class="nav-main-link-name">Jenis Resume</span>
            </a>
            <a class="nav-main-link active" href="#">
                <i class="nav-main-link-icon  far fa-file fa-2x "></i>
                <span class="nav-main-link-name">Resume</span>
            </a>
            
            <a class="nav-main-link active" href="#">
                <i class="nav-main-link-icon  far fa-folder-open  fa-2x "></i>
                <span class="nav-main-link-name"> Portfolio</span>
            </a>
            <a class="nav-main-link active" href="#">
                <i class="nav-main-link-icon  far fa-hdd  fa-2x "></i>
                <span class="nav-main-link-name"> Berkas</span>
            </a>
            <a class="nav-main-link active" href="{{route('keluar')}}">
                <i class="nav-main-link-icon  si si-logout  fa-2x "></i>
                <span class="nav-main-link-name"> Logout</span>
            </a>
        </li>
    </ul>
</div>
@endsection
@section('utama')

    {{-- //jenis profil --}}
   
        <div class="col-md-3">
            <table class="table table-bordered ">
                <h3>Type Resume</h3>
                <thead>
                    <th>ID Produk</th>
                    <th>Nama</th>
                 
                </thead>
                <tbody>
                   @foreach ($type as $v)
                       <tr>
                           <td>{{$v->id_resume}}</td>
                           <td>{{$v->jenis_resume}}</td>
                       </tr>
                   @endforeach
                </tbody>
            </table>  
        </div>
  
   <div class="center">
        <table class="table table-bordered ">
            <h3>Profil</h3>
            <thead>
                <th>Kode Resume</th>
                <th>Nama</th>
                <th>Website</th>
                <th>Tanggal Lahir</th>
                <th>Menu</th>
            </thead>
            <tbody>
               @foreach ($profil as $v)
                   <tr>
                       <td>{{$v->kode_resume}}</td>
                       <td>{{$v->nama}}/{{$v->nama_jabatan}}</td>
                       <td>{{$v->alamat_web}}</td>
                       <td>{{$v->tanggal_lahir}}</td>
                       <td>
                           <a href=""><i class="icon  fa fa-edit  "></i></a>
                           <a href=""><i class="icon  fa fa-trash "></i></a>
                       </td>
                   </tr>
               @endforeach
            </tbody>
        </table>  
    </div>
       
        {{-- modal profil --}}
        <div class="modal fade" id="modal-block-popin" tabindex="-1" role="dialog" aria-labelledby="modal-block-popin" aria-hidden="true">
            <div class="modal-dialog modal-dialog-popin modal-xl" role="document">
                <div class="modal-content">
                
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Profil</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-fw fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content font-size-sm">
                    {!! Form::open(array('url'=>'profil')) !!}
                            <table class="table">
                                <tr>
                                    <td>Nama/名前</td>
                                    <td><input type="text" class="form-control" name="nama"></td>
                                </tr>
                                <tr>
                                    <td>Nama Skill</td>
                                    <td><input type="text" class="form-control" name="nama_jabatan"></td>
                                </tr>
                                <tr>
                                    <td>No Tlp</td>
                                    <td><input type="text" class="form-control" name="notlp"></td>
                                </tr>
                                <tr>
                                    <td>Alamat Website</td>
                                    <td><input type="text" class="form-control" name="alamat_web"></td>
                                </tr>
                                <tr>
                                    <td>Tentang Saya</td>
                                   <td>
                                    <textarea name="tentang_saya" class="form-control" id="" cols="30" rows="10"></textarea>
                                   </td>
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir</td>
                                    <td><input type="text" class="form-control" name="tanggal_lahir"></td>
                                </tr>
                                <tr>
                                    <td>Jenis kelamin</td>
                                    <td><input type="text" class="form-control" name="jenis_kelamin"></td>
                                </tr>
                                <tr>
                                    <td>Bahasa</td>
                                    <td><input type="text" class="form-control" name="bahasa"></td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>
                                        <textarea name="alamat" class="form-control" id="" cols="30" rows="10"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kemampuan</td>
                                    <td>
                                        <textarea name="kemampuan" class="form-control" id="" cols="30" rows="10"></textarea>
                                    </td>
                                </tr>
                            </table>
                       
                        </div>
                        <div class="block-content block-content-full text-right border-top">
                            <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check mr-1">ok</i></button>
                        </div>
                        {!! Form::close() !!}      
                    </div>
                </div>
            </div>
        </div>
     {{-- jenis resune --}}
     <div class="modal fade" id="modal-block-jenis" tabindex="-1" role="dialog" aria-labelledby="modal-block-popin" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
            
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Jenis Resume</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                {!! Form::open(array('url'=>'jenisresume')) !!}
                       <table class="table">
                           <tr>
                               <td>
                                   <p>Id Resume</p>
                                   <input type="text" class="form-control" name="id_resume">
                               </td>
                               <td>
                                <p>Nama Resume</p>
                                <input type="text" class="form-control" name="jenis_resume">
                            </td>
                           </tr>
                       </table>
                   
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check mr-1">ok</i></button>
                    </div>
                    {!! Form::close() !!}      
                </div>
            </div>
        </div>
    </div>
        
      
@endsection

{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
