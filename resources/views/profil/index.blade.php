@extends('layouts.page')
@section('center')
     
<button type="button" class="btn btn-success mr-1 mb-3" data-toggle="modal" data-target="#modal-block-popout">
    <i class="fa fa-fw fa-plus mr-1"></i> Add Resum
</button>

<div class="col-md-12">
<table class="table">
    <thead>
        <th>Kode Resume</th>
        <th>Nama/name/名前</th>
        <th>web/website/ウェブサイト</th>
        <th>Menu</th>
    </thead>
    <tbody>
       @foreach ($profil as $v)
           <tr>
               <td>{{$v->kode_resume}}</td>
                <td>{{$v->nama}}</td>
                <td>{{$v->alamat_web}}</td>
                <td colspan="2">
                    <div class="col-sm-6 col-lg-4 col-xl-3">
                        <p><i class="far fa-edit fa-2x"></i></p>
                        <p><i class="far fa-trash-alt fa-2x"></i></p>
                    </div>
                   
                </td>
           </tr>
       @endforeach
    </tbody>
</table>
</div>
<div class="modal fade" id="modal-block-popout" tabindex="-1" role="dialog" aria-labelledby="modal-block-popout" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout modal-lg" role="document">
    {!! Form::open(array('url'=>'profil')) !!}
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Create New Resume</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection