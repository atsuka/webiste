<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('keluar');
Route::get('profil', 'ProfilController@index')->name('profil.index');
Route::get('profil/create', 'ProfilController@create')->name('profil.create');
Route::post('profil', 'ProfilController@store')->name('profil.store');
Route::post('jenisresume', 'HomeController@jenisresume')->name('resume.store');
Route::get('resume/{id}', 'HomeController@profil')->name('resume.id');