<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profils', function (Blueprint $table) {
            $table->id();
            $table->integer('kode_resume');
            $table->string('nama');
            $table->string('nama_jabatan');
            $table->string('notlp');
            $table->string('alamat_web');
            $table->text('tentang_saya');
            $table->string('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('bahasa');
            $table->text('alamat');
            $table->text('kemampuan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profils');
    }
}
