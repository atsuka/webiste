<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profil;

class ProfilController extends Controller
{
    public function index()
    {
        $data['profil'] = Profil::all();
        return view('profil.index', $data);
    }
    public function create()
    {
        return view('profil.create');
    }
    public function store(Request $req)
    {
        
        $data = new Profil;
        $data->nama =$req->nama;
        $data->nama_jabatan = $req->nama_jabatan;
        $data->notlp = $req->notlp;
        $data->alamat_web = $req->alamat_web;
        $data->kemampuan = $req->kemampuan;
        $data->tentang_saya = $req->tentang_saya;
        $data->tanggal_lahir = $req->tanggal_lahir;
        $data->jenis_kelamin =$req->jenis_kelamin;
        $data->bahasa = $req->bahasa;
        $data->alamat =$req->alamat;
        $data->save();
        return back();
    }
}
