<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profil;
use App\jenisResume;
use App\TypeResume;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['profil'] = Profil::all();
        $data['type'] = TypeResume::all();
        return view('home', $data);
    }
    public function jenisresume(Request $req)
    {
       
        $data = new TypeResume;
        $data->id_resume = $req->id_resume;
        $data->jenis_resume = $req->jenis_resume;
        $data->save();
        return back();
    }
    public function profil($id)
    {
        $data = \DB::table('type_resumes')
            ->join('profils', 'profils.kode_resume', '=', 'type_resumes.id_resume')
            ->where('id_resume', $id)
             ->get();
        return $data;
    }
}
