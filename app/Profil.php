<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profils';
    protected $fillable =[
        'nama',
        'nama_jabatan',
        'notlp',
        'alamat_web',
        'tentang_saya',
        'tanggal_lahir',
        'jenis_kelamin',
        'bahasa',
        'alamat',
        'kemampuan',
    ];
}
