<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeResume extends Model
{
    protected $table = 'type_resumes';
    protected $fillable = [
        'id_resume',
        'jenis_resume',
    ];
}
